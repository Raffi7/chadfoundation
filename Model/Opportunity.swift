//
//  Product.swift
//  ChadFoundation
//
//  Created by Rafael Courtney McLeary on 11/24/19.
//  Copyright © 2019 Mundane Soft LLC. All rights reserved.
//

import Foundation

struct Opportunity {
    private(set) public var title: String
    private(set) public var imageName: String
    private(set) public var desc: String
    
    init(title: String, imageName: String, desc: String) {
        self.title = title
        self.imageName = imageName
        self.desc = desc
    }
    
}
