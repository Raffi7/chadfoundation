//
//  DataService.swift
//  ChadFoundation
//
//  Created by Rafael Courtney McLeary on 11/24/19.
//  Copyright © 2019 Mundane Soft LLC. All rights reserved.
//

import Foundation

class DataService {
    static let instance = DataService()
    
    private let categories = [
        Category(title: "ART", imageName: "Newarkart.JPG"),
        Category(title: "CITY SERVICE", imageName: "Civil.jpg"),
        Category(title: "CLEAN UP", imageName: "Trash.jpg"),
        Category(title: "SHELTER", imageName: "Soupkitchen.jpg")
    ]
    
    func getCategories() -> [Category] {
        return categories
    }
    
    private let opportunities = [
        Opportunity(title: "Mayor's Office: Street Tree Steward", imageName: "trees.png", desc: "Street trees are a critical element of building Newark’s sustainability and resiliency by beautifying neighborhoods, and reducing air pollution, storm water runoff, and urban heat. By inventorying our street trees, we will better understand the health of our urban forestry, and use that information to manage it more effectively. We are recruiting residents, school groups, faith groups, and community-based organizations to complete specialized training to help inventory street trees their neighborhood."),
        Opportunity(title: "Mayor's Office: Sustainable Stormwater Stewards", imageName: "rain.png", desc: "Catch basins are designed to collect water during storm events. When a storm occurs, rainwater carries litter, debris and other pollutants to catch basins on the street. By adopting a catch basin, residents can help beautify their neighborhood, reduce localized flooding and help reduce the amount of litter and debris getting into our pipes and waterways. We are recruiting Newark residents, school groups, faith groups, and community-based organizations to adopt catch basins near them. Volunteers will be provided with a free “catch basin care kit."),
        Opportunity(title: "Mayor's Office: Newark Learning Collaborative", imageName: "fafsa.png", desc: "As a part of the Newark FAFSA Challenge, we are in need of volunteers to assist Newark high school seniors and their parents in completing the Free Application for Federal Student Aid (FAFSA). Upcoming FAFSA Assistance Volunteer Training Dates: Wednesday, November 20, 2019 – 6 pm to 7:30 pm \n Monday, November 25, 2019 – 6 pm to 7:30 pm \n Saturday, December 7, 2019 – 11 am to 12:30 pm \n Essex County College Training, Inc. (4th floor), 303 University Ave, Newark, NJ 07102"),
    ]
    
    func getOpportunities() -> [Opportunity] {
        return opportunities
    }
}
