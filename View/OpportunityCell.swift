//
//  OpportunityCell.swift
//  ChadFoundation
//
//  Created by Rafael Courtney McLeary on 11/26/19.
//  Copyright © 2019 Mundane Soft LLC. All rights reserved.
//

import UIKit

class OpportunityCell: UITableViewCell {
    @IBOutlet weak var opImage: UIImageView!
    @IBOutlet weak var opTitle: UILabel!
    @IBOutlet weak var opDesc: UITextView!
    
    
    func updateViews(opportunity: Opportunity) {
        opImage.image = UIImage(named: opportunity.imageName)
        opTitle.text = opportunity.title
        opDesc.text = opportunity.desc
        
    }
}
