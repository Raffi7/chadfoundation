//
//  TextFields.swift
//  ChadFoundation
//
//  Created by Rafael Courtney McLeary on 11/24/19.
//  Copyright © 2019 Mundane Soft LLC. All rights reserved.
//

import UIKit

@IBDesignable
class TextFields: UITextField {
    

        override func prepareForInterfaceBuilder() {
            customizedView()
        }
        
        override func awakeFromNib() {
            super.awakeFromNib()
            customizedView()
        }
        
        func customizedView() {
            backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.25)
            layer.cornerRadius = 5.0
            textAlignment = .center

            if let p = placeholder {
                let place = NSAttributedString(string: p, attributes: [.foregroundColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)])
                attributedPlaceholder = place
                textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            
        }
        
    }

