//
//  CategoryCell.swift
//  ChadFoundation
//
//  Created by Rafael Courtney McLeary on 11/24/19.
//  Copyright © 2019 Mundane Soft LLC. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    func updateViews(category: Category) {
        categoryImage.image = UIImage(named: category.imageName)
        categoryTitle.text = category.title
    }
    
}
