//
//  InfoVC.swift
//  ChadFoundation
//
//  Created by Rafael Courtney McLeary on 11/24/19.
//  Copyright © 2019 Mundane Soft LLC. All rights reserved.
//

import UIKit

class InfoVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var infoTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        infoTable.dataSource = self
        infoTable.delegate = self
        infoTable.backgroundColor = UIColor.clear
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataService.instance.getOpportunities().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "OpportunityCell") as? OpportunityCell {
            cell.contentView.backgroundColor = UIColor.clear
            let opportunity = DataService.instance.getOpportunities()[indexPath.row]
            cell.updateViews(opportunity: opportunity)
            
            return cell
        } else {
            return OpportunityCell()
        }
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        //hides keyboard when screen is tapped outside of keyboard
    }
    
    
}
